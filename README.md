# OpenML dataset: fps_benchmark

https://www.openml.org/d/44834

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This dataset contains FPS measurement of video games executed on computers. Each row of the dataset describes the outcome of FPS measurement (outcome is attribute FPS) for a video game executed on a computer. A computer is characterized by the CPU and the GPU. For both the name is resolved to technical specifications (features starting with Cpu and Gpu).

The technical specification of CPU and GPU are technical specification that describe the factory state of the respective component.

The game is characterized by the name, the displayed resolution, and the quality setting that was adjusted during the measurement (features starting with Game).

From the original data, only those observations are considered which *Dataset* feature's value is *fps-benchmark*, then the column is removed.

**Attribute Description**

CPU

1. *CpuNumberOfCores* - number of physical cores
2. *CpuNumberOfThreads* - number of threads
3. *CpuBaseClock* - base clock in Mhz
4. *CpuCacheL1* - total size of level 1 cache in kB
5. *CpuCacheL2* - total size of level 2 cache in kB
6. *CpuCacheL3* - total size of level 3 cache in MB
7. *CpuDieSize* - physical size of the die in square meter
8. *CpuFrequency* - frequency in Mhz
9. *CpuMultiplier* - multiplier of Cpu
10. *CpuMultiplierUnlocked* - 0=multiplier locked, 1=multiplier unlocked
11. *CpuProcessSize* - used process size in nanometer
12. *CpuTDP* - thermal design power in watt
13. *CpuNumberOfTransistors* - count of transistors in million
14. *CpuTurbo Clock* - turbo clock in Mhz
15. *CpuName*

GPU

16. *GpuBandwidth* bandwidth in MB/s
17. *GpuBaseClock* base clock in MHz
18. *GpuBoostClock* boost clock in MHz
19. *GpuNumberOfComputeUnits* number of computing units
20. *GpuDieSize* physical size of die in square meter
21. *GpuNumberOfExecutionUnits* number of execution units
22. *GpuFP32Performance* theoretical Float 32 performance in MFLOP/s
23. *GpuMemoryBus* width of memory bus in bits
24. *GpuMemorySize* size of memory in MB
25. *GpuPixelRate* theoretical pixel rate in MPixel/s
26. *GpuProcessSize* used process size in nanometer
27. *GpuNumberOfROPs* number of render output units
28. *GpuNumberOfShadingUnits* number of shading units
29. *GpuNumberOfTMUs* number of texture mapping units
30. *GpuTextureRate* theoretical texture rate in KTexel/s
31. *GpuNumberOfTransistors* number of transistors in million
32. *GpuArchitecture* architecture code
33. *GpuMemoryType* memory type
34. *GpuOpenCL* version of OpenCL
35. *GpuShaderModel* version of shader model
36. *GpuVulkan* version of Vulkan
37. *GpuOpenGL* version of OpenGL
38. *GpuName*
39. *GpuBus.interface* - bus interface
40. *GpuDirectX*

GAME

41. *GameName*
42. *GameResolution*
43. *GameSetting*
44. *FPS* - target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44834) of an [OpenML dataset](https://www.openml.org/d/44834). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44834/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44834/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44834/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

